/**
 * Created by thetkhine on 12/27/18.
 */
import React, { Component } from 'react';
import {
    Image,
    StyleSheet,
    Dimensions,
    Text,
    TextInput,
    View,
    ImageBackground,
    InteractionManager,
    Alert,
    AsyncStorage,
    TouchableOpacity,

} from 'react-native';
import {
    Button as NButton,
    Text as NText,
    Switch} from 'native-base';
const { width } = Dimensions.get('window');

import DatePicker from 'react-native-datepicker';
import RNPickerSelect from 'react-native-picker-select';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview';
import withBusyIndicator from '../../components/WithBusyIndicator';
const KeyboardAwareScrollViewWithBusyIndicator = withBusyIndicator(KeyboardAwareScrollView);
class CarTab extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {

            reservationStart:"",
            reservationEnd:"",
            category: '',


        };
    }
    changeCategory(value)
    {
        this.setState({
            category: value
        });


    }
    render()
    {
        var categories = [
            {
                label:"Category1",
                value : 0
            },
            {
                label:"Category 2",
                value : 2
            }];
        return(<KeyboardAwareScrollViewWithBusyIndicator style={styles.mainContainer}
                                                         contentContainer={styles.contentContainer}>
            <View style={styles.headerContainer}>
                    <Text style={styles.headerText}>
                        Find a Car
                    </Text>
            </View>
            <View style={styles.container}>

                <RNPickerSelect style={{...pickerSelectStyles}}
                                iosHeader="Select Category"
                                mode="dropdown"
                                value={this.state.category}
                                placeholder={{
                                    label: 'Select Category',
                                    value: null,}}
                                onValueChange={this.changeCategory.bind(this)}
                                items={categories}
                                ref={(el) => {
                                    this.picker = el;
                                }}
                                onUpArrow={() => {
                                    this.picker.togglePicker();
                                }}
                                onDownArrow={() => {
                                    this.picker.focus();
                                }}
                >


                </RNPickerSelect>
            </View>
            <View style={styles.container}>

                <DatePicker
                    style={{width:'100%'}}
                    date={this.state.reservationStart}
                    mode="date"
                    placeholder="Reservation Start"
                    format="YYYY-MM-DD"
                    minDate="2016-05-01"
                    maxDate="2016-06-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 0
                        }
                        // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => {this.setState({reservationStart: date})}}
                />
            </View>
            <View style={styles.container}>

                <DatePicker
                    style={{width:'100%'}}
                    date={this.state.reservationEnd}
                    mode="date"
                    placeholder="Reservation End"
                    format="YYYY-MM-DD"
                    minDate="2016-05-01"
                    maxDate="2016-06-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            right: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 0
                        }
                        // ... You can check the source to find the other keys.
                    }}
                    onDateChange={(date) => {this.setState({reservationEnd: date})}}
                />
            </View>
            <View style={styles.container}>
                <Switch

                    onValueChange={ (value) => this.setState({ sameAsPickup: value })}
                    value={this.state.sameAsPickup}/>
                <Text style={{marginLeft:10}}>
                    Return same as Pick-up?
                </Text>
            </View>
            <View style={styles.rowContainer}>

            </View>
                </KeyboardAwareScrollViewWithBusyIndicator>)
    }
}
var styles = StyleSheet.create({

    mainContainer:{
        flex:1,
        backgroundColor:'white',
        padding:10,
        flexDirection:'column',

    },
    headerContainer:{
        flex:1,
        /*width:'80%',*/
        flexDirection: 'row',
        marginBottom:15,
        marginTop:15,
        paddingLeft: width * 0.1,/*ten percentage of width */
        paddingRight: width * 0.1,
        alignItems:'center',
        justifyContent:'flex-start',
    },
    container:{
        flex:1,
        /*width:'80%',*/
        flexDirection: 'column',
        marginBottom:10,
        paddingLeft: width * 0.1,/*ten percentage of width */
        paddingRight: width * 0.1,
        alignItems:'center',
        justifyContent:'center',
        /*backgroundColor:'green',*/
    },
    contentContainer:{
        alignItems:'center',
        justifyContent:'center',
    },
    headerText:{
        fontSize:18,
        fontWeight:'bold'
    },
    rowContainer:{
        flex:1,
        width:'100%',
        flexDirection: 'row',
        marginBottom:15,
        alignItems:'center',
    },
    colContainer:{
        flex:1,
        width:'100%',
        flexDirection: 'row',

    },
    rightContainer:{
        justifyContent:'flex-end',
    },



});
const pickerSelectStyles = StyleSheet.create({

    inputIOS: {
        flex:1,
        width:'100%',
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderRadius: 4,
        borderColor:'lightgray',
        color: 'black',
    },
});
export default CarTab;